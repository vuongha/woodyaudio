/*
 * GBBBattery.h
 *
 *  Created on: May 24, 2019
 *      Author: TA QUOC ANH
 */

#ifndef COMPONENTS_BATTERY_GBBBATTERY_H_
#define COMPONENTS_BATTERY_GBBBATTERY_H_

class GBBBattery {
public:
	GBBBattery();
	virtual ~GBBBattery();


	/**
	 * @brief Get capacity of batterys
	 *
	 * @return Voltage of battery
	 *
	 */
	float getCapacity(void);


	/**
	 * @brief Show capacity with light. Show capacity of battery currently
	 *
	 * @param NULL
	 *
	 * @return NULL
	 */
	void showCapacity(void);
private:


	static void monitorBATT(void *param);

};



extern GBBBattery RobotBattery;

#endif /* COMPONENTS_BATTERY_GBBBATTERY_H_ */
