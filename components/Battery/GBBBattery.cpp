/*
 * GBBBattery.cpp
 *
 *  Created on: May 24, 2019
 *      Author: TA QUOC ANH
 */

#include "freertos/FreeRTOS.h"
#include "sdkconfig.h"
#include "freertos/task.h"
#include <esp_log.h>
#include <string>

#include "GBBLightRGB.h"
#include "driver/adc.h"
#include "nvs.h"
#include "nvs_flash.h"

#include "GBBMotor.h"
#include "GBBBattery.h"
#include "driver/adc.h"
#include "esp_adc_cal.h"
#include "esp_bt_main.h"
#include "esp_bt.h"

#define BOARD_78     0
#define BOARD_66    1

#define BOARD BOARD_78
//#define BOARD BOARD_66

GBBBattery::GBBBattery() {

	xTaskCreate(monitorBATT, " Monitor BATT", 4096, NULL, 3, NULL);

}

GBBBattery::~GBBBattery() {
	// TODO Auto-generated destructor stub
}

void GBBBattery::monitorBATT(void *param) {

	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(ADC1_CHANNEL_3, ADC_ATTEN_DB_11);
	float value = 0;
	float capacity;
	int counter = 0;
	while (1) {
		if (RobotWheelBody.isRuning() == false) {
			vTaskDelay(100 / portTICK_PERIOD_MS);
			for (int i = 0; i < 50; i++) {
				value = value + adc1_get_raw(ADC1_CHANNEL_3);
			}
			value = value / 50;
#if (BOARD == BOARD_78)
			capacity = (value / 4096) * 7.2;

#elif (BOARD == BOARD_66)
			capacity = (value / 4096) * 6.6;
#endif
			value = 0;
			vTaskDelay(300 / portTICK_PERIOD_MS);
			if (RobotWheelBody.isRuning() == true) {
				capacity = 4;
			}
			if (capacity < 3.67)
				counter++;
			if (counter == 10) {
				RobotLED.runRGB(GO_SLEEP, 2);
				RobotLED.stopMagic();
				vTaskDelay(10000/portTICK_PERIOD_MS);
				printf("shutdown esp32 \n");
				esp_deep_sleep_start();
				vTaskDelay(500 / portTICK_PERIOD_MS);
				vTaskSuspend(NULL);
			}
		}
		vTaskDelay(3000 / portTICK_PERIOD_MS);
	}
}

void GBBBattery::showCapacity(void) {

	vTaskDelay(500 / portTICK_PERIOD_MS);
	adc1_config_width(ADC_WIDTH_BIT_12);
	adc1_config_channel_atten(ADC1_CHANNEL_3, ADC_ATTEN_DB_11);
	float value = 0;
	float capacity = 0;
	for (int i = 0; i < 10; i++) {
		value = value + adc1_get_raw(ADC1_CHANNEL_3);
		vTaskDelay(10 / portTICK_PERIOD_MS);
	}
	value = value / 10;
#if (BOARD == BOARD_74)
	capacity = (value / 4096) * 7.2;

#elif (BOARD == BOARD_66)
	capacity = (value / 4096) * 6.6;
#endif
	value = 0;
	printf(" value BATT: %.2f V\n", capacity);
	if (capacity >= 3.8) {
		RobotLED.runRGB(GOOD_BATT, 2);
	} else if (capacity >= 3.69 && capacity < 3.8) {
		RobotLED.runRGB(NORMAL_BATT, 2);
	} else if (capacity < 3.69) {
		RobotLED.runRGB(GO_SLEEP, 2);
		while (1)
			;
	}
	RobotLED.waitcomplete();
	vTaskDelay(400 / portTICK_PERIOD_MS);
}

float GBBBattery::getCapacity(void) {
	static float value = 0;
	static float capacity;
	for (int i = 0; i < 50; i++) {
		value = value + adc1_get_raw(ADC1_CHANNEL_3);
		vTaskDelay(5 / portTICK_PERIOD_MS);
	}
	value = value / 50;
#if (BOARD == BOARD_74)
	capacity = (value / 4096) * 7.2;
#elif (BOARD == BOARD_66)
	capacity = (value / 4096) * 6.6;
#endif
	value = 0;
	return capacity;
}


GBBBattery RobotBattery;
