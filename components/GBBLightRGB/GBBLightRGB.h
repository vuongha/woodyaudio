/*
 * GBBLightRGB.h
 *
 *  Created on: Oct 8, 2018
 *      Author: Mr Vuong
 */

#ifndef GBBLIGHTRGB_GBBLIGHTRGB_H_
#define GBBLIGHTRGB_GBBLIGHTRGB_H_

#include <stdint.h>
#include <driver/rmt.h>
#include <driver/gpio.h>
#include <freertos/FreeRTOS.h>

#include <freertos/task.h>
#include "freertos/semphr.h"

typedef struct {
	uint8_t red;
	uint8_t green;
	uint8_t blue;
} pixel_t;

typedef enum {
	ADVERIESING = 1,
	CONNECTED = 2,
	DISCONNECTED = 3,
	COMPLETE = 4,
	RED = 5,
	GREEN = 6,
	BLUE = 7,
	YELLOW = 8,
	GOOD_BATT = 9,
	NORMAL_BATT = 10,
	BAD_BATT = 11,
	MOVE_FORWARD = 12,
	MOVE_BACKWARD = 13,
	TURN_RIGHT = 14,
	TURN_LEFT = 15,
	GO_SLEEP = 16,
	DANCING = 17,
	SHOWLED = 18,
	NO_CONNECT = 19,
	NO_WORK

} type_show;

class GBBLightRGB {

public:
	GBBLightRGB();

	void show(void);  // display on LED

	void setColorOrder(char *colorOrder);

	void setPixel(uint32_t index, uint8_t red, uint8_t green, uint8_t blue);

	void setPixel(uint32_t index, pixel_t pixel);

	void clear(void);


	void startMagic(void);

	void stopMagic(void);

	void stopRGBCurrent(void);

	void waitcomplete(void);

	void runRGB(type_show type, int time);

	void runRGB_random(void);

	void end_RGB_random(void);


	type_show image;

	int time;

	TaskHandle_t isRunLed;

	TaskHandle_t isRunMagic;

	virtual ~GBBLightRGB();

private:
	char *colorOrder;
	uint16_t pixelCount;
	rmt_channel_t channel;
	rmt_item32_t *items;
	pixel_t *pixels;

};

extern GBBLightRGB RobotLED;

#endif /* GBBLIGHTRGB_GBBLIGHTRGB_H_ */
