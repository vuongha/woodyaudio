/*
 * GBBLightRGB.cpp
 *
 *  Created on: Oct 8, 2018
 *      Author: Mr Vuong
 */

#include "GBBLightRGB.h"

#include <freertos/FreeRTOS.h>

#include <freertos/task.h>

#include <esp_log.h>
#include <driver/rmt.h>
#include <driver/gpio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdexcept>
#include "sdkconfig.h"
#include <esp_heap_caps.h>

static char tag[] = "WS2812";
SemaphoreHandle_t isCompletedLight;
bool flag_stop_RGB_current = false;
static void SetItem1(rmt_item32_t *pItem) {
	assert(pItem != nullptr);
	pItem->level0 = 1;
	pItem->duration0 = 10;
	pItem->level1 = 0;
	pItem->duration1 = 6;
}

static void SetItem0(rmt_item32_t *pItem) {
	assert(pItem != nullptr);
	pItem->level0 = 1;
	pItem->duration0 = 4;
	pItem->level1 = 0;
	pItem->duration1 = 8;
}

static void SetTerminator(rmt_item32_t *pItem) {
	assert(pItem != nullptr);
	pItem->level0 = 0;
	pItem->duration0 = 0;
	pItem->level1 = 0;
	pItem->duration1 = 0;
}

static uint8_t getChannelValueByType(char type, pixel_t pixel) {
	switch (type) {
	case 'r':
	case 'R':
		return pixel.red;
	case 'b':
	case 'B':
		return pixel.blue;
	case 'g':
	case 'G':
		return pixel.green;
	}
	ESP_LOGW(tag, "Unknown color channel 0x%2x", type);
	return 0;
}

GBBLightRGB RobotLED;

static void runRGBLed(void *param);

void static run_magic(void * param);

GBBLightRGB::GBBLightRGB() {

	this->pixelCount = 4;
	this->channel = RMT_CHANNEL_0;
	this->items = new rmt_item32_t[8 * 24 + 1];
	this->pixels = new pixel_t[4];
	this->colorOrder = (char *) "GRB";
	this->time = 0;
	this->image = RED;

	rmt_config_t config_rmt_module;

	config_rmt_module.rmt_mode = RMT_MODE_TX;
	config_rmt_module.channel = this->channel; // 8 pixel
	config_rmt_module.gpio_num = GPIO_NUM_25; // GPIO18 is to control RGB LED  //vuong
	config_rmt_module.mem_block_num = 8 - this->channel;
	config_rmt_module.clk_div = 8;
	config_rmt_module.tx_config.loop_en = 0;
	config_rmt_module.tx_config.carrier_en = 0;
	config_rmt_module.tx_config.idle_output_en = 1;
	config_rmt_module.tx_config.idle_level = (rmt_idle_level_t) 0;
	config_rmt_module.tx_config.carrier_freq_hz = 10000;
	config_rmt_module.tx_config.carrier_level = (rmt_carrier_level_t) 1;
	config_rmt_module.tx_config.carrier_duty_percent = 50;

	ESP_ERROR_CHECK(rmt_config(&config_rmt_module));
	ESP_ERROR_CHECK(rmt_driver_install(this->channel, 0, 0));

	xTaskCreate(runRGBLed, "run led", 8192, NULL, 4, &RobotLED.isRunLed);
	xTaskCreate(run_magic, "run normal", 4096, NULL, 2, &RobotLED.isRunMagic);
	isCompletedLight = xSemaphoreCreateBinary();

}

void GBBLightRGB::show(void) {
	auto pCurrentItem = this->items;

	for (auto i = 0; i < this->pixelCount; i++) {
		uint32_t currentPixel = (getChannelValueByType(this->colorOrder[0],
				this->pixels[i]) << 16)
				| (getChannelValueByType(this->colorOrder[1], this->pixels[i])
						<< 8)
				| (getChannelValueByType(this->colorOrder[2], this->pixels[i]));

		//	ESP_LOGD(tag, "Pixel value: %x", currentPixel);
		for (int j = 23; j >= 0; j--) {
			// We have 24 bits of data representing the red, green amd blue channels. The value of the
			// 24 bits to output is in the variable current_pixel.  We now need to stream this value
			// through RMT in most significant bit first.  To do this, we iterate through each of the 24
			// bits from MSB to LSB.
			if (currentPixel & (1 << j)) {
				SetItem1(pCurrentItem);
			} else {
				SetItem0(pCurrentItem);
			}
			pCurrentItem++;
		}
	}
	SetTerminator(pCurrentItem); // Write the RMT terminator.

	// Show the pixels.
	ESP_ERROR_CHECK(
			rmt_write_items(this->channel, this->items, this->pixelCount * 24,
					1 /* wait till done */));

}

void GBBLightRGB::setColorOrder(char *colorOrder) {

	if (colorOrder != nullptr && strlen(colorOrder) == 3) {
		this->colorOrder = colorOrder;
	}

}

void GBBLightRGB::setPixel(uint32_t index, uint8_t red, uint8_t green,
		uint8_t blue) {

	assert(index < 4);
	this->pixels[index].red = red;
	this->pixels[index].green = green;
	this->pixels[index].blue = blue;

}

void GBBLightRGB::setPixel(uint32_t index, pixel_t pixel) {

	assert(index < 4);

	this->pixels[index] = pixel;

}


void GBBLightRGB::clear(void) {

	for (int i = 0; i <= 3; i++) {
		this->pixels[i].red = 0;
		this->pixels[i].green = 0;
		this->pixels[i].blue = 0;
	}
}

GBBLightRGB::~GBBLightRGB() {

	delete this->items;
	delete this->pixels;

} /* namespace Gobot */

void GBBLightRGB::runRGB(type_show type_, int count) {

	if (eTaskGetState(isRunLed) == eSuspended) {
		image = type_;
		time = count;
		vTaskSuspend(RobotLED.isRunMagic);
		vTaskResume(RobotLED.isRunLed);
	}
}

void GBBLightRGB::runRGB_random(void) {

	int red = 5 + rand() % 150;
	int green = 5 + rand() % 100;
	int blue = 5 + rand() % 200;

	if (eTaskGetState(isRunLed) == eSuspended) {
		vTaskSuspend(isRunMagic);
	}
	RobotLED.setPixel(0, 00, 0, 00);
	RobotLED.setPixel(1, 0, 0, 00);
	RobotLED.setPixel(2, 00, 0, 00);
	RobotLED.setPixel(3, 00, 0, 00);

	RobotLED.show();

	RobotLED.setPixel(0, red, green, blue);
	RobotLED.setPixel(1, red, green, blue);
	RobotLED.setPixel(2, red, green, blue);
	RobotLED.setPixel(3, red, green, blue);

	RobotLED.show();

}

void GBBLightRGB::end_RGB_random(void) {
	RobotLED.clear();
	RobotLED.show();
	vTaskResume(isRunMagic);

}

void GBBLightRGB::startMagic(void) {

	vTaskResume(isRunMagic);
}

void GBBLightRGB::stopMagic(void) {
	vTaskSuspend(isRunMagic);
	clear();
	show();
}

void GBBLightRGB::waitcomplete(void) {

	xSemaphoreTake(isCompletedLight, portMAX_DELAY);
}
void GBBLightRGB::stopRGBCurrent(void) {
	flag_stop_RGB_current = true;
}

static void runRGBLed(void *param) {
	vTaskSuspend(NULL);
	for (;;) {

		switch (RobotLED.image) {

		case ADVERIESING: // run advertising
			for (int i = 0; i < RobotLED.time; i++) {
				RobotLED.setPixel(0, 100, 0, 0);
				RobotLED.setPixel(1, 0, 0, 0);
				RobotLED.setPixel(2, 0, 0, 0);
				RobotLED.setPixel(3, 0, 0, 0);

				RobotLED.show();
				vTaskDelay(50);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 0, 0, 0);
				RobotLED.setPixel(3, 0, 0, 0);

				RobotLED.show();
				vTaskDelay(50);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 100, 50, 50);
				RobotLED.setPixel(3, 0, 0, 0);

				RobotLED.show();
				vTaskDelay(50);

				RobotLED.setPixel(0, 100, 100, 0);
				RobotLED.setPixel(1, 100, 100, 0);
				RobotLED.setPixel(2, 100, 50, 50);
				RobotLED.setPixel(3, 100, 10, 40);

				RobotLED.show();
				vTaskDelay(50);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(50);

			}
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);
			break;

		case RED: // con connected

			for (int i = 0; i < RobotLED.time; i++) {

				RobotLED.setPixel(0, 100, 0, 00);
				RobotLED.setPixel(1, 100, 0, 00);
				RobotLED.setPixel(2, 100, 0, 00);
				RobotLED.setPixel(3, 100, 0, 00);

				RobotLED.show();

			}
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);

			break;

		case YELLOW:

			for (int i = 0; i < RobotLED.time; i++) {

				RobotLED.setPixel(0, 200, 50, 00);
				RobotLED.setPixel(1, 200, 50, 00);
				RobotLED.setPixel(2, 200, 50, 00);
				RobotLED.setPixel(3, 200, 50, 00);

				RobotLED.show();

			}
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);

			break;

		case DISCONNECTED:

			for (int i = 0; i < 1; i++) {
				i++;

				RobotLED.setPixel(0, 100, 0, 0);
				RobotLED.setPixel(1, 100, 0, 0);
				RobotLED.setPixel(2, 100, 0, 0);
				RobotLED.setPixel(3, 100, 0, 0);

				RobotLED.show();
				vTaskDelay(10);
			}
			vTaskDelay(300 / portTICK_PERIOD_MS);
			vTaskSuspend(NULL);
			break;
		case COMPLETE:

			for (int i = 0; i < RobotLED.time; i++) {

				RobotLED.setPixel(0, 0, 0, 255);
				RobotLED.setPixel(1, 0, 0, 255);
				RobotLED.setPixel(2, 0, 0, 255);
				RobotLED.setPixel(3, 0, 0, 255);

				RobotLED.show();
				vTaskDelay(20);
				RobotLED.clear();
				RobotLED.show();

				RobotLED.setPixel(0, 0, 150, 0);
				RobotLED.setPixel(1, 0, 150, 0);
				RobotLED.setPixel(2, 0, 150, 0);
				RobotLED.setPixel(3, 0, 150, 0);

				RobotLED.show();
				vTaskDelay(20);
				RobotLED.clear();
				RobotLED.show();

				RobotLED.setPixel(0, 150, 0, 0);
				RobotLED.setPixel(1, 150, 0, 0);
				RobotLED.setPixel(2, 150, 0, 0);
				RobotLED.setPixel(3, 150, 0, 0);

				RobotLED.show();
				vTaskDelay(20);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(20);
			}
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);
			break;

		case GREEN:

			for (int i = 0; i < RobotLED.time; i++) {
				RobotLED.setPixel(0, 0, 100, 00);
				RobotLED.setPixel(1, 0, 100, 00);
				RobotLED.setPixel(2, 0, 100, 00);
				RobotLED.setPixel(3, 0, 100, 00);

				RobotLED.show();
			}
			vTaskDelay(3000 / portTICK_PERIOD_MS);
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);
			break;

		case BLUE:

			for (int i = 0; i < RobotLED.time; i++) {
				RobotLED.setPixel(0, 0, 0, 100);
				RobotLED.setPixel(1, 0, 0, 100);
				RobotLED.setPixel(2, 0, 0, 100);
				RobotLED.setPixel(3, 0, 0, 100);

				RobotLED.show();
			}
			vTaskDelay(3000 / portTICK_PERIOD_MS);
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);

			break;

		case GOOD_BATT:

			RobotLED.setPixel(0, 0, 100, 00);
			RobotLED.setPixel(1, 0, 100, 00);
			RobotLED.setPixel(2, 0, 100, 00);
			RobotLED.setPixel(3, 0, 100, 00);

			RobotLED.show();
			vTaskDelay(2000 / portTICK_PERIOD_MS);
			RobotLED.clear();
			RobotLED.show();
			vTaskResume(RobotLED.isRunMagic);
			xSemaphoreGive(isCompletedLight);
			vTaskSuspend(NULL);
			break;

		case NORMAL_BATT:

			RobotLED.setPixel(0, 200, 50, 00);
			RobotLED.setPixel(1, 200, 50, 00);
			RobotLED.setPixel(2, 200, 50, 00);
			RobotLED.setPixel(3, 200, 50, 00);

			RobotLED.show();
			vTaskDelay(2000 / portTICK_PERIOD_MS);
			RobotLED.clear();
			RobotLED.show();
			vTaskResume(RobotLED.isRunMagic);
			xSemaphoreGive(isCompletedLight);
			vTaskSuspend(NULL);
			break;

		case BAD_BATT:

			RobotLED.setPixel(0, 100, 0, 00);
			RobotLED.setPixel(1, 100, 0, 00);
			RobotLED.setPixel(2, 100, 0, 00);
			RobotLED.setPixel(3, 100, 0, 00);

			RobotLED.show();
			vTaskDelay(2000 / portTICK_PERIOD_MS);
			RobotLED.clear();
			RobotLED.show();
			vTaskResume(RobotLED.isRunMagic);
			xSemaphoreGive(isCompletedLight);
			vTaskSuspend(NULL);
			break;

		case MOVE_BACKWARD:
			for (int i = 0; i < 17; i++) {
				RobotLED.setPixel(0, 100, 0, 100);
				RobotLED.setPixel(1, 00, 0, 00);
				RobotLED.setPixel(2, 00, 0, 00);
				RobotLED.setPixel(3, 00, 0, 00);

				RobotLED.show();
				vTaskDelay(100 / portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(100 / portTICK_PERIOD_MS);
			}
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);
			break;
		case MOVE_FORWARD:
			for (int i = 0; i < 17; i++) {
				RobotLED.setPixel(0, 00, 0, 00);
				RobotLED.setPixel(1, 0, 0, 00);
				RobotLED.setPixel(2, 100, 00, 100);
				RobotLED.setPixel(3, 00, 0, 00);

				RobotLED.show();
				vTaskDelay(100 / portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(100 / portTICK_PERIOD_MS);
			}
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);
			break;

		case TURN_LEFT:
			for (int i = 0; i < 10; i++) {
				RobotLED.setPixel(0, 00, 0, 00);
				RobotLED.setPixel(1, 100, 0, 100);
				RobotLED.setPixel(2, 0, 0, 00);
				RobotLED.setPixel(3, 00, 0, 00);

				RobotLED.show();
				vTaskDelay(100 / portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(100 / portTICK_PERIOD_MS);
			}
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);
			break;
		case TURN_RIGHT:
			for (int i = 0; i < 10; i++) {
				RobotLED.setPixel(0, 00, 0, 00);
				RobotLED.setPixel(1, 00, 0, 00);
				RobotLED.setPixel(2, 00, 0, 00);
				RobotLED.setPixel(3, 100, 0, 100);

				RobotLED.show();
				vTaskDelay(100 / portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(100 / portTICK_PERIOD_MS);
			}
			vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);
			break;

		case GO_SLEEP:
			while (1) {
				RobotLED.setPixel(0, 100, 0, 00);
				RobotLED.setPixel(1, 100, 0, 00);
				RobotLED.setPixel(2, 100, 0, 00);
				RobotLED.setPixel(3, 100, 0, 00);

				RobotLED.show();
				vTaskDelay(220 / portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(80 / portTICK_PERIOD_MS);

				RobotLED.setPixel(0, 100, 0, 00);
				RobotLED.setPixel(1, 100, 0, 00);
				RobotLED.setPixel(2, 100, 0, 00);
				RobotLED.setPixel(3, 100, 0, 00);

				RobotLED.show();
				vTaskDelay(220 / portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(80 / portTICK_PERIOD_MS);

				RobotLED.setPixel(0, 100, 0, 00);
				RobotLED.setPixel(1, 100, 0, 00);
				RobotLED.setPixel(2, 100, 0, 00);
				RobotLED.setPixel(3, 100, 0, 00);

				RobotLED.show();
				vTaskDelay(220 / portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(80 / portTICK_PERIOD_MS);

				RobotLED.setPixel(0, 100, 0, 00);
				RobotLED.setPixel(1, 100, 0, 00);
				RobotLED.setPixel(2, 100, 0, 00);
				RobotLED.setPixel(3, 100, 0, 00);

				RobotLED.show();
				vTaskDelay(220 / portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(80 / portTICK_PERIOD_MS);

				vTaskDelay(1500 / portTICK_PERIOD_MS);

			}
			//vTaskResume(RobotLED.isRunMagic);
			vTaskSuspend(NULL);

			break;

		case DANCING:

			for (int i = 0; i < 3; i++) {
				RobotLED.setPixel(0, 100, 0, 00);
				RobotLED.setPixel(1, 100, 0, 00);
				RobotLED.setPixel(2, 100, 0, 00);
				RobotLED.setPixel(3, 100, 0, 100);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 00, 0, 100);
				RobotLED.setPixel(1, 00, 0, 100);
				RobotLED.setPixel(2, 00, 0, 100);
				RobotLED.setPixel(3, 00, 0, 100);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 00, 100, 00);
				RobotLED.setPixel(1, 00, 100, 00);
				RobotLED.setPixel(2, 00, 100, 00);
				RobotLED.setPixel(3, 00, 100, 00);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 100, 0, 50);
				RobotLED.setPixel(1, 100, 0, 50);
				RobotLED.setPixel(2, 100, 0, 50);
				RobotLED.setPixel(3, 100, 0, 50);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 50, 0, 100);
				RobotLED.setPixel(1, 50, 0, 100);
				RobotLED.setPixel(2, 50, 0, 100);
				RobotLED.setPixel(3, 50, 0, 100);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 100, 50, 00);
				RobotLED.setPixel(1, 100, 50, 00);
				RobotLED.setPixel(2, 100, 50, 00);
				RobotLED.setPixel(3, 100, 50, 00);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

// doan 2

				RobotLED.setPixel(0, 50, 100, 00);
				RobotLED.setPixel(1, 50, 100, 00);
				RobotLED.setPixel(2, 50, 100, 00);
				RobotLED.setPixel(3, 50, 100, 00);

				RobotLED.show();
				vTaskDelay(6 * 4);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

			}
// doan 3

			RobotLED.setPixel(0, 80, 100, 200);
			RobotLED.setPixel(1, 80, 100, 200);
			RobotLED.setPixel(2, 80, 100, 200);
			RobotLED.setPixel(3, 80, 100, 210);

			RobotLED.show();
			vTaskDelay(6 * 4);
			RobotLED.clear();
			RobotLED.show();
			vTaskDelay(6 * 1);

			RobotLED.setPixel(0, 100, 0, 00);
			RobotLED.setPixel(1, 100, 0, 00);
			RobotLED.setPixel(2, 100, 0, 00);
			RobotLED.setPixel(3, 100, 0, 00);

			RobotLED.show();
			vTaskDelay(6 * 4);
			RobotLED.clear();
			RobotLED.show();
			vTaskDelay(6 * 1);

			RobotLED.setPixel(0, 00, 100, 00);
			RobotLED.setPixel(1, 00, 100, 00);
			RobotLED.setPixel(2, 00, 100, 00);
			RobotLED.setPixel(3, 00, 100, 00);

			RobotLED.show();
			vTaskDelay(6 * 4);
			RobotLED.clear();
			RobotLED.show();
			vTaskDelay(6 * 1);

			RobotLED.setPixel(0, 00, 0, 200);
			RobotLED.setPixel(1, 00, 0, 200);
			RobotLED.setPixel(2, 00, 0, 200);
			RobotLED.setPixel(3, 00, 0, 200);

			RobotLED.show();
			vTaskDelay(6 * 4);
			RobotLED.clear();
			RobotLED.show();
			vTaskDelay(6 * 1);

			vTaskResume(RobotLED.isRunMagic);
			xSemaphoreGive(isCompletedLight);
			vTaskSuspend(NULL);
			break;

		case SHOWLED:
			for (int i = 0; i < 4; i++) {

				RobotLED.setPixel(0, 100, i * 50, 00);
				RobotLED.setPixel(1, 100, i * 50, 00);
				RobotLED.setPixel(2, 100, i * 50, 00);
				RobotLED.setPixel(3, 100, i * 50, 100);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 00, 0, i * 50);
				RobotLED.setPixel(1, 00, 0, i * 50);
				RobotLED.setPixel(2, 00, 0, i * 50);
				RobotLED.setPixel(3, 00, 0, i * 50);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, i * 50, 100, 00);
				RobotLED.setPixel(1, i * 50, 100, 00);
				RobotLED.setPixel(2, i * 50, 100, 00);
				RobotLED.setPixel(3, i * 50, 100, 00);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 100, i * 50, 50);
				RobotLED.setPixel(1, 100, i * 50, 50);
				RobotLED.setPixel(2, 100, i * 50, 50);
				RobotLED.setPixel(3, 100, i * 50, 50);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 50, i * 50, 100);
				RobotLED.setPixel(1, 50, i * 50, 100);
				RobotLED.setPixel(2, 50, i * 50, 100);
				RobotLED.setPixel(3, 50, i * 50, 100);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				RobotLED.setPixel(0, 100, 50, i * 50);
				RobotLED.setPixel(1, 100, 50, i * 50);
				RobotLED.setPixel(2, 100, 50, i * 50);
				RobotLED.setPixel(3, 100, 50, i * 50);

				RobotLED.show();
				vTaskDelay(6 * 2);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

// doan 2

				RobotLED.setPixel(0, 50, 100, 00);
				RobotLED.setPixel(1, 50, 100, 00);
				RobotLED.setPixel(2, 50, 100, 00);
				RobotLED.setPixel(3, 50, 100, 00);

				RobotLED.show();
				vTaskDelay(6 * 4);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);
			}

			vTaskResume(RobotLED.isRunMagic);
			xSemaphoreGive(isCompletedLight);
			vTaskSuspend(NULL);

			break;

		case NO_CONNECT:

			flag_stop_RGB_current = false;

			while (1) {

				RobotLED.setPixel(0, 150, 20, 150);
				RobotLED.setPixel(1, 00, 00, 00);
				RobotLED.setPixel(2, 0, 00, 00);
				RobotLED.setPixel(3, 0, 00, 0);

				RobotLED.show();
				vTaskDelay(6 * 4);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);
				if (flag_stop_RGB_current == true)
					break;
				RobotLED.setPixel(0, 00, 0, 00);
				RobotLED.setPixel(1, 150, 20, 150);
				RobotLED.setPixel(2, 00, 0, 00);
				RobotLED.setPixel(3, 00, 0, 00);

				RobotLED.show();
				vTaskDelay(6 * 4);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				if (flag_stop_RGB_current == true)
					break;

				RobotLED.setPixel(0, 0, 00, 00);
				RobotLED.setPixel(1, 0, 00, 00);
				RobotLED.setPixel(2, 150, 20, 150);
				RobotLED.setPixel(3, 0, 00, 0);

				RobotLED.show();
				vTaskDelay(6 * 4);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);

				if (flag_stop_RGB_current == true)
					break;
				RobotLED.setPixel(0, 00, 0, 00);
				RobotLED.setPixel(1, 00, 0, 00);
				RobotLED.setPixel(2, 00, 0, 00);
				RobotLED.setPixel(3, 150, 20, 150);

				RobotLED.show();
				vTaskDelay(6 * 4);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(6 * 1);
				if (flag_stop_RGB_current == true)
					break;

			}
			vTaskResume(RobotLED.isRunMagic);
			xSemaphoreGive(isCompletedLight);
			vTaskSuspend(NULL);
			break;
		case NO_WORK:
			while(1){

				RobotLED.setPixel(0, 100, 0, 00);
				RobotLED.setPixel(1, 100, 0, 00);
				RobotLED.setPixel(2, 100, 0, 00);
				RobotLED.setPixel(3, 100, 0, 00);

				RobotLED.show();
				vTaskDelay(200/portTICK_PERIOD_MS);
				RobotLED.clear();
				RobotLED.show();
				vTaskDelay(500/portTICK_PERIOD_MS);
			}
			break;

		default:
			break;
		}
	}
}

void static run_magic(void * param) {
	short red = 150;
	short blue = 0;
	short green = 180;
	vTaskSuspend(NULL);
	for (;;) {

		RobotLED.setPixel(0, 168, 10, 38);
		RobotLED.setPixel(1, 168, 10, 38);
		RobotLED.setPixel(2, 168, 10, 38);
		RobotLED.setPixel(3, 168, 10, 38);

		RobotLED.show();
		vTaskDelay(100);

		RobotLED.setPixel(0,10, 34, 173);
		RobotLED.setPixel(1,10, 34, 173);
		RobotLED.setPixel(2, 10, 34, 173);
		RobotLED.setPixel(3, 10, 34, 173);

		RobotLED.show();
		vTaskDelay(100);

		RobotLED.setPixel(0, 5, 117, 39);
		RobotLED.setPixel(1, 5, 117, 39);
		RobotLED.setPixel(2, 5, 117, 39);
		RobotLED.setPixel(3, 5, 117, 39);

		RobotLED.show();
		vTaskDelay(100);

		RobotLED.setPixel(0, 120, 3, 124);
		RobotLED.setPixel(1, 120, 3, 124);
		RobotLED.setPixel(2, 120, 3, 124);
		RobotLED.setPixel(3, 120, 3, 124);

		RobotLED.show();
		vTaskDelay(100);

		RobotLED.setPixel(0, 130, 1, 57);
		RobotLED.setPixel(1, 130, 1, 57);
		RobotLED.setPixel(2, 130, 1, 57);
		RobotLED.setPixel(3, 130, 1, 57);

		RobotLED.show();
		vTaskDelay(100);

		RobotLED.setPixel(0, 29, 216, 19);
		RobotLED.setPixel(1, 29, 216, 19);
		RobotLED.setPixel(2, 29, 216, 19);
		RobotLED.setPixel(3, 29, 216, 19);

		RobotLED.show();
		vTaskDelay(100);

		RobotLED.setPixel(0, 7, 148, 183);
		RobotLED.setPixel(1, 7, 148, 183);
		RobotLED.setPixel(2, 7, 148, 183);
		RobotLED.setPixel(3, 7, 148, 183);

		RobotLED.show();
		vTaskDelay(100);

	}
}

