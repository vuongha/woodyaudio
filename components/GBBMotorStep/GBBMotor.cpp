
/*
 * GBBMotor.cpp
 *
 *  Created on: Mar 24, 2019
 *      Author: TA QUOC ANH
 */

#include "GBBMotor.h"
#include "AccelStepper.h"
#include "nvs.h"
#include "nvs_flash.h"
#include "esp_err.h"
#define HALFSTEP 8

#define MOVEAHEAD 2794	  // Step to move 15 cm
#define TURN  990     // Step to turn 90 degree

#define SPACE_STORAGE "woody"
#define NVS_CONFIG_ANGLE "angle"

SemaphoreHandle_t isCompleted;
SemaphoreHandle_t Semaphore_I2C_A;
SemaphoreHandle_t Semaphore_I2C_B;
xTaskHandle Task_Handle_Step;
xTaskHandle Task_Handle_Step_Left;

// Initialize with pin sequence IN1-IN3-IN2-IN4 for using the AccelStepper with 28BYJ-48
//Board_74

AccelStepper Motor_Right(HALFSTEP, 0, 2, 1, 3);

AccelStepper Motor_Left(HALFSTEP, 4, 6, 5, 7);

//

//Board_66
//AccelStepper Motor_Right(HALFSTEP, motorPin2, motorPin4, motorPin1, motorPin3);
//
//AccelStepper Motor_Left(HALFSTEP, motorPin5, motorPin7, motorPin6, motorPin8);

static void Task_Run_Step(void *param) {

	vTaskSuspend(NULL);
	for (;;) {
		Motor_Right.run();
		Motor_Left.run();
		if ((Motor_Right.isRunning() == false) // khong chay nua
		&& (Motor_Left.isRunning() == false)) {
			Motor_Left.disableOutputs();
			Motor_Right.disableOutputs();
			xSemaphoreGive(isCompleted);
			vTaskSuspend(Task_Handle_Step);
		}
	}
}


GBBMotor::GBBMotor() {
	// TODO Auto-generated constructor stub

	stepTurn = TURN;
	Motor_Right.setMaxSpeed(1000);
	Motor_Right.setAcceleration(1000.0);
	Motor_Left.setMaxSpeed(1000);
	Motor_Left.setAcceleration(1000.0);
	isCompleted = xSemaphoreCreateBinary();
	xTaskCreatePinnedToCore(Task_Run_Step, "Task_StepMotor", 2*4096, NULL, 2,
			&Task_Handle_Step,1);
}

void GBBMotor::setStepTurn(int stepTurn) {
	this->stepTurn = stepTurn;
}

long GBBMotor::getStepTurn(void) {
	return (long) this->stepTurn;
}

void GBBMotor::turnRight(void) {

	Motor_Right.move(-stepTurn);
	Motor_Left.move(stepTurn);
	Motor_Right.setSpeed(-1000);
	Motor_Left.setSpeed(1000);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}

void GBBMotor::turnLeft(void) {

	Motor_Right.move(stepTurn);
	Motor_Left.move(-stepTurn);
	Motor_Right.setSpeed(1000);
	Motor_Left.setSpeed(-1000);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}

void GBBMotor::turnLeft_45(void) {

	Motor_Right.move(480);
	Motor_Left.move(-480);
	Motor_Right.setSpeed(1000);
	Motor_Left.setSpeed(-1000);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}

void GBBMotor::turnRight_45(void) {

	Motor_Right.move(-480);
	Motor_Left.move(480);
	Motor_Right.setSpeed(-1000);
	Motor_Left.setSpeed(1000);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}

void GBBMotor::moveForward(void) {

	Motor_Right.move((MOVEAHEAD));
	Motor_Left.move((MOVEAHEAD));
	Motor_Right.setSpeed(1000);
	Motor_Left.setSpeed(1000);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}

void GBBMotor::moveBackward(void) {

	Motor_Right.move(-(MOVEAHEAD));
	Motor_Left.move(-(MOVEAHEAD));
	Motor_Right.setSpeed(-1000);
	Motor_Left.setSpeed(-1000);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}

void GBBMotor::moveBackwardDistance(int distance) {
	Motor_Right.move(-distance * 186);
	Motor_Left.move(-distance * 186);
	Motor_Right.setSpeed(-1000);
	Motor_Left.setSpeed(-1000);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}

void GBBMotor::moveForwardDistance(int distance) {
	Motor_Right.move(distance * 186);
	Motor_Left.move(distance * 186);
	Motor_Right.setSpeed(1000);
	Motor_Left.setSpeed(1000);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}


void GBBMotor::turnRightAngle(int angle) {
	Motor_Right.move((long)((-angle*stepTurn)/90));
	Motor_Left.move((long)((angle*stepTurn)/90));
	float speed = (angle*stepTurn)/90;
	if(speed > 1000) speed = 1000;
	Motor_Right.setSpeed(-speed);
	Motor_Left.setSpeed(speed);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}

void GBBMotor::turnLeftAngle(int angle) {
	Motor_Right.move((long)((angle*stepTurn)/90));
	Motor_Left.move((long)((-angle*stepTurn)/90));
	float speed = (angle*stepTurn)/90;
	if(speed > 1000) speed = 1000;
	Motor_Right.setSpeed(speed);
	Motor_Left.setSpeed(-speed);
	vTaskResume(Task_Handle_Step);
	xSemaphoreTake(isCompleted, portMAX_DELAY);

}


bool GBBMotor::isRuning(void) {
	return (Motor_Left.isRunning() == true) && (Motor_Right.isRunning() == true);
}

void GBBMotor::stop(void) {
	Motor_Right.stop();
	Motor_Left.stop();
}

GBBMotor::~GBBMotor() {
	// TODO Auto-generated destructor stub
}

GBBMotor RobotWheelBody;
