
/*
 * GBBMotor.h
 *
 *  Created on: Mar 24, 2019
 *      Author: TA QUOC ANH
 */

#ifndef COMPONENTS_GBBMOTORSTEP_GBBMOTOR_H_
#define COMPONENTS_GBBMOTORSTEP_GBBMOTOR_H_
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "freertos/semphr.h"
#include "esp_attr.h"
#include "soc/rtc.h"
#include "driver/mcpwm.h"
#include "soc/mcpwm_reg.h"
#include "soc/mcpwm_struct.h"
#include  <driver/timer.h>
#include "soc/timer_group_struct.h"

class GBBMotor {
public:
	GBBMotor();

	/**
	 * @brief Turn right robot 90 degree
	 *
	 * @param NULL
	 *
	 * @return NULL
	 *
	 */
	void turnRight(void);


	/**
	 * @brief Turn left Robot 90 degree
	 *
	 * @param NULL
	 *
	 * @return NULL
	 */
	void turnLeft(void);

	/**
	 * @brief Turn left Robot 45 degree
	 *
	 * @param NULL
	 *
	 * @return NULL
	 */

	void turnLeft_45(void) ;


	/**
	 * @brief Turn right Robot 45 degree
	 *
	 * @pram NULL
	 *
	 * @return NULL
	 */

	void turnRight_45(void) ;


	/**
	 * @brief Move robot 15 cm in Map
	 *
	 * @param NULL
	 *
	 * @return NULL
	 */

	void moveForward(void);


	/**
	 * @brief Move robot 15 cm in Map
	 *
	 * @param NULL
	 *
	 * @return NULL
	 */

	void moveBackward(void);

	/**
	 * @brief Move Robot fackward distance
	 *
	 * @param[in] distance (cm) Distance value to move :
	 *
	 * @return NULL
	 */

	void moveForwardDistance(int distance);


	/**
	 * @brief Move Robot backward with distance
	 *
	 * @param[in] distance (cm) Distance value to move :
	 *
	 * @return NULL
	 */

	void moveBackwardDistance(int distance);


	/**
	 * @brief Turn right angle with angle
	 *
	 * @pram[in] angle  Angle for turn
	 *
	 * @return - NULL
	 */
	void turnRightAngle(int angle);

	/**
	 * @brief      Turn left robot with angle
	 *
	 * @param[in]      angle  Angle for turn
	 *
	 * @return
	 *     - NULL
	 */

	void turnLeftAngle(int angle);


	/**
	 * @brief     Stop Robot
	 *
	 * @return  NULL
	 */

	void stop(void);


	/**
	 * @brief    Check robot moving or not
	 *
	 * @return
	 *
	 *		- true : Robot  run
	 *		- false: Robot  stop
	 *
	 */

	bool isRuning(void);


	/**
	 * @brief    Set step for Robot to turn
	 *
	 * @param[in]   stepTurn Step
	 *
	 * @return
	 *
	 * 		- NULL
	 */

	void setStepTurn(int stepTurn);

	/**
	 * @brief    Get step turn
	 *
	 * @param NULL
	 *
	 * @return Step for turn
	 */

	long getStepTurn(void);

	virtual ~GBBMotor();
private:

	uint16_t stepTurn;
};


extern GBBMotor RobotWheelBody ;
#endif /* COMPONENTS_GBBMOTORSTEP_GBBMOTOR_H_ */
