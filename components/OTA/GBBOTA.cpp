/*
 * GBBOTA.cpp
 *
 *  Created on: May 24, 2019
 *      Author: TA QUOC ANH
 */

#include "GBBOTA.h"
#include "errno.h"
#include "sdkconfig.h"
GBBOTA::GBBOTA() {
	// TODO Auto-generated constructor stub

}

GBBOTA::~GBBOTA() {
	// TODO Auto-generated destructor stub
}

void GBBOTA::config_wifi(std::string ssid, std::string pass) {
	// Open


	ESP_LOGD(STO_TAG,"\n");
		ESP_LOGI(STO_TAG,"Opening Non-Volatile Storage (NVS) handle... ");

	nvs_handle my_handle;
	esp_err_t err = nvs_open_from_partition("nvs", "mystore", NVS_READWRITE,
			&my_handle);

	if (err != ESP_OK) {
		//ESP_LOGD(STO_TAG,"Error (%s) opening NVS handle!\n", esp_err_to_name(err));
	} else {
		ESP_LOGD(STO_TAG, "Done\n");
		//read
//------

		if (ssid != "0") {
			size_t required_size;
			nvs_get_str(my_handle, "wifissid", NULL, &required_size);
			char* wifissid = (char*) malloc(required_size);
//	    	server_name[required_size]='\0';
			err = nvs_get_str(my_handle, "wifissid", wifissid, &required_size);
			switch (err) {
			case ESP_OK:
				ESP_LOGD(STO_TAG, "Done\n")
				;
				ESP_LOGI(STO_TAG, "wifissid = %s, lenght = %d \n", wifissid,
						required_size)
				;
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				ESP_LOGI(STO_TAG, "The value is not initialized yet!\n")
				;
				break;
			default:
				break;
				// 	ESP_LOGD(STO_TAG,"Error (%s) reading!\n", esp_err_to_name(err));
			}
			//write
			err = nvs_set_str(my_handle, "wifissid", ssid.c_str());
			printf((err != ESP_OK) ? "Failed!\n" : "Done key\n");
		}
		//read

		if (pass  != "0") {
			size_t required_size_pass;
			nvs_get_str(my_handle, "wifipass", NULL, &required_size_pass);
			char* wifipass = (char*) malloc(required_size_pass);
//	    	server_name[required_size]='\0';
			err = nvs_get_str(my_handle, "wifipass", wifipass,
					&required_size_pass);
			switch (err) {
			case ESP_OK:
				ESP_LOGD(STO_TAG, "Done\n")
				;
				ESP_LOGI(STO_TAG, "wifipass = %s, lenght = %d \n", wifipass,
						required_size_pass)
				;
				break;
			case ESP_ERR_NVS_NOT_FOUND:
				ESP_LOGI(STO_TAG, "The value is not initialized yet!\n")
				;
				break;
			default:
				break;
				//	ESP_LOGI(STO_TAG,"Error (%s) reading!\n", esp_err_to_name(err));
			};

			err = nvs_set_str(my_handle, "wifipass", pass.c_str());
			printf((err != ESP_OK) ? "Failed!\n" : "Done value\n");

		}
		// Commit written value.
		// After setting any values, nvs_commit() must be called to ensure changes are written
		// to flash storage. Implementations may write to storage at other times,
		// but this is not guaranteed.

		printf("Committing updates in NVS ... ");
		err = nvs_commit(my_handle);
		printf((err != ESP_OK) ? "Failed!\n" : "Done\n");

		// Close
		nvs_close(my_handle);
	}

	printf("\n");
}



void GBBOTA::execute_ota(void) {
	if (1) {
		const esp_partition_t* nvs_partition = esp_partition_find_first(
				ESP_PARTITION_TYPE_APP, ESP_PARTITION_SUBTYPE_APP_FACTORY,
				NULL);
		printf(" type %d", (int) nvs_partition->type);
		printf("subtype %d ", (int) nvs_partition->subtype);
		printf("address %d", (uint32_t) nvs_partition->address);
		printf("size %d", (uint32_t) nvs_partition->size);

		if (!nvs_partition)
			printf("FATAL ERROR: No NVS partition found\n");
		esp_err_t err = esp_ota_set_boot_partition(nvs_partition);
		if (err != ESP_OK) {
		}
		ESP_LOGI("PIKING", "Prepare to restart system!");

	} else {

	}
	esp_restart();
	//***********************/*
}


GBBOTA RobotOta;
