/*
 * GBBOTA.h
 *
 *  Created on: May 24, 2019
 *      Author: TA QUOC ANH
 */

#ifndef COMPONENTS_OTA_GBBOTA_H_
#define COMPONENTS_OTA_GBBOTA_H_

#include <esp_log.h>
#include <string>
#include "esp_err.h"
#include <stdlib.h>


#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_ota_ops.h"

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"




#include "esp_partition.h"


#include "nvs.h"
#include "nvs_flash.h"
#include "nvs.h"
#include "nvs_flash.h"

#define STO_TAG "OTA"

class GBBOTA {
public:
	GBBOTA();
	virtual ~GBBOTA();

	void config_wifi(std::string ssid, std::string pass);

	void execute_ota(void);

	void store_SSID(std::string ssid);

};


extern GBBOTA RobotOta;
#endif /* COMPONENTS_OTA_GBBOTA_H_ */
