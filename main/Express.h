/*
 * Express.h
 *
 *  Created on: Oct 21, 2019
 *      Author: TA QUOC ANH
 */

#ifndef MAIN_EXPRESS_H_
#define MAIN_EXPRESS_H_

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>


typedef struct {
	int id;
	int param1;
	int param2;
} movement_param_t;

typedef struct {
	char *name;
	int dance_param;
	int move_param;
	int light_param;
} audio_param_t;


typedef enum {
	NFC = 0, BLE = 1

} source_t;

typedef enum {
	AUDIO = 0, MOVEMENT = 1
} action_t;

//typedef enum {
//	WAIT = 0
//} status_nfc_t;
//
//typedef enum {
//	BUSY =0
//
//} status_ble_t;

typedef struct {
	movement_param_t movement_papram;
	audio_param_t audio_param;
	source_t source;
	action_t action;
//	status_nfc_t status_nfc;
//	status_ble_t status_ble;
} event_msg_t;



class Express {
public:
	Express();
	virtual ~Express();
	void send_event(event_msg_t *event);
	void listen_event(event_msg_t *event);

private:
	event_msg_t even_smg;

	xQueueHandle queue;

};

extern Express Message;

#endif /* MAIN_EXPRESS_H_ */
