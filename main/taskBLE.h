/*
 * taskBLE.h
 *
 *  Created on: Oct 16, 2019
 *      Author: TA QUOC ANH
 */

#ifndef MAIN_TASKBLE_H_
#define MAIN_TASKBLE_H_

class taskBLE {
public:
	taskBLE();
	virtual ~taskBLE();
};


void start_ble_services(void);
void start_task_ble(void);
#endif /* MAIN_TASKBLE_H_ */
