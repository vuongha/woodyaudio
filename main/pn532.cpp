/*
 * pn532.cpp
 *
 *  Created on: Jul 2, 2019
 *      Author: TA QUOC ANH
 */

#include <Wire.h>
#include <SPI.h>
#include <Adafruit_PN532.h>
#include <vector>
#include <sstream>
#include <string>
#include "esp_log.h"
#define PN532_SCK  (18)
#define PN532_MOSI (23)
#define PN532_SS   (17)
#define PN532_MISO (19)
#define TAG "NFC"

Adafruit_PN532 nfc(PN532_SCK, PN532_MISO, PN532_MOSI, PN532_SS);

void test_pn532(void *param) {
	Serial.begin(115200);
	Serial.println("Hello!");

	nfc.begin();
	delay(10);
	uint32_t versiondata = nfc.getFirmwareVersion();
	if (!versiondata) {
		Serial.print("Didn't find PN53x board");
		while (1)
			; // halt
	}

	Serial.print("Found chip PN5");
	Serial.println((versiondata >> 24) & 0xFF, HEX);
	Serial.print("Firmware ver. ");
	Serial.print((versiondata >> 16) & 0xFF, DEC);
	Serial.print('.');
	Serial.println((versiondata >> 8) & 0xFF, DEC);

	// configure board to read RFID tags
	nfc.SAMConfig();

	Serial.println("Waiting for an ISO14443A Card ...");

	while (1) {

		uint8_t success;
		uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 }; // Buffer to store the returned UID
		uint8_t uidLength = 4; // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
		uint8_t blockNumber = 4; // Counter to keep track of which block we're on
		bool authenticated = false;
		uint8_t data[16];
		uint8_t keyuniversal[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

		success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,
				blockNumber, 0, keyuniversal);
		if (!success) {
//			Serial.println("Blocked here 1 \n");
			success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid,
					&uidLength,20);
//			Serial.println("Blocked here 2 \n");
			success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,
					blockNumber, 0, keyuniversal);

		}
		if (success) {
			uint8_t data[16];
			for (int i = 0; i < 2; i++) {
				success = nfc.mifareclassic_ReadDataBlock(blockNumber + i,
						data);
				if (success) {
					nfc.PrintHexChar(data, 16);

				} else {
//					ESP_LOGI(TAG,
//							"Ooops ... unable to read the requested block.  Try another key?");
				}
			}
		} else {
//			ESP_LOGI(TAG, "Ooops ... authentication failed: Try another key?");
		}
//		nfc.shutDown();
//		Serial.println("Shutdown");
		vTaskDelay(150/portTICK_PERIOD_MS);
	}

}

void shutdown_pn532(void *param) {

	nfc.shutDown();

}


void test_pn532_b(void *param){

	Serial.begin(115200);
	Serial.println("Hello!");

	nfc.begin();
	delay(10);
	uint32_t versiondata = nfc.getFirmwareVersion();
	if (!versiondata) {
		Serial.print("Didn't find PN53x board");
		while (1)
			; // halt
	}

	Serial.print("Found chip PN5");
	Serial.println((versiondata >> 24) & 0xFF, HEX);
	Serial.print("Firmware ver. ");
	Serial.print((versiondata >> 16) & 0xFF, DEC);
	Serial.print('.');
	Serial.println((versiondata >> 8) & 0xFF, DEC);

	// configure board to read RFID tags
	nfc.SAMConfig();

	Serial.println("Waiting for an ISO14443A Card ...");

	while (1) {

		uint8_t success;
		uint8_t uid[] = { 0, 0, 0, 0, 0, 0, 0 }; // Buffer to store the returned UID
		uint8_t uidLength = 4; // Length of the UID (4 or 7 bytes depending on ISO14443A card type)
		uint8_t blockNumber = 4; // Counter to keep track of which block we're on
		bool authenticated = false;
		uint8_t data[16];
		uint8_t keyuniversal[6] = { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

		success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,
				blockNumber, 0, keyuniversal);
		if (!success) {
			Serial.println("Blocked here 1 \n");
			success = nfc.readPassiveTargetID(PN532_MIFARE_ISO14443A, uid,
					&uidLength, 50);
			Serial.println("Blocked here 2 \n");
			success = nfc.mifareclassic_AuthenticateBlock(uid, uidLength,
					blockNumber, 0, keyuniversal);
		}
		if (success) {
			uint8_t data[16];
			for (int i = 0; i < 2; i++) {
				success = nfc.mifareclassic_ReadDataBlock(blockNumber + i,
						data);
				if (success) {
					nfc.PrintHexChar(data, 16);
				} else {
					ESP_LOGI(TAG,
							"Ooops ... unable to read the requested block.  Try another key?");
				}
			}
		} else {
			ESP_LOGI(TAG, "Ooops ... authentication failed: Try another key?");
		}
		nfc.shutDown();
		vTaskDelay(3000/portTICK_PERIOD_MS);
	}
}
