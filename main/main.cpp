/* Play MP3 file from SD Card

 This example code is in the Public Domain (or CC0 licensed, at your option.)

 Unless required by applicable law or agreed to in writing, this
 software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 CONDITIONS OF ANY KIND, either express or implied.
 */
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"


#include "esp_log.h"
#include "nvs_flash.h"
#include "sdkconfig.h"
#include "audio_element.h"
#include "audio_pipeline.h"
#include "audio_event_iface.h"
#include "audio_common.h"
#include "fatfs_stream.h"
#include "i2s_stream.h"
#include "mp3_decoder.h"

#include "esp_peripherals.h"
#include "periph_sdcard.h"
#include "board.h"

#include "driver/sdmmc_host.h"
#include "sdmmc_cmd.h"
#include "esp_vfs_fat.h"

#include "GBBOTA.h"
#include "GBBMotor.h"
#include "PCF8574.h"
#include "GBBMotor.h"
#include "taskBLE.h"
#include "taskFNC.h"
#include "manage.h"
#include "Arduino.h"
#include "Audio.h"

extern "C" {
void app_main(void);
}

extern void test_pn532(void *param);
extern void shutdown_pn532(void *param);
static void main_program(void *param);
void start_main_program(void);
xTaskHandle PN532handle;

//void find_index_block(char *param, int len, int *index) {
//	int i = 0;
//	for (i = 0; i < len; i++) {
//		if (param[i] == ';') {
//			*(index) = i;
//			index = index + 1;
//		}
//	}
//}
//
//movement_param_t* process_data(char *ptr_char, int len, int *num_block) {
//
//	char char_value[1];
//	static char *array_block;
//	int num_of_block = 0;
//
//	array_block = ptr_char;
//
//	for (int i = 0; i < len; i++) {
//		if (array_block[i] == ';')
//			num_of_block++;
//	}
//	num_of_block--;
////	printf("Num of block %d \n", num_of_block);
////	if (num_of_block < 3) {
////		flag_repeat = false;
////		flag_stop = true;
////		return;
////	}
//
//	int *index_block = (int*) calloc(num_of_block, sizeof(int));
//	movement_param_t *movement_array = (movement_param_t*) calloc(num_of_block,
//			sizeof(movement_param_t));
//	printf(" num block %d \n", num_of_block);
//
//	find_index_block((char*) array_block, len, index_block);
//
//	for (int i = 0; i < num_of_block; i++) {
//		printf("iiiadi %d\n", i);
//		sscanf((char*) (array_block + index_block[i] + 1), "%d,%d,%d",
//				&movement_array[i].id, &movement_array[i].param1,
//				&movement_array[i].param2);
//	}
//
//	*num_block = num_of_block;
//	return movement_array;
//
//}

void shutdown_task(void *param){
	while(1){
		vTaskSuspend(PN532handle);
		shutdown_pn532(NULL);
		Serial.println("Shutdown");
		vTaskDelay(5000/portTICK_PERIOD_MS);
		Serial.println("resume");
		vTaskResume(PN532handle);
		vTaskDelay(5000/portTICK_PERIOD_MS);
	}
}

char data[] = "100;100,200,200;200,300,400;500,600,700;300";

Audio Speaker;

void app_main(void) {
	Speaker.beginAudio();
	vTaskDelay(1000/portTICK_PERIOD_MS);
	printf("play mp3\n");

	xTaskCreate(test_pn532, "read", 4*1024, NULL, 1, &PN532handle);
	vTaskDelay(1000/portTICK_PERIOD_MS);
	start_ble_services();
	start_task_ble();
	start_main_program();
	vTaskDelay(2000/portTICK_PERIOD_MS);
	Speaker.playNextMp3();

	RobotWheelBody.moveForwardDistance(10000);

//	vTaskDelay(1000/portTICK_PERIOD_MS);
//	xTaskCreate(shutdown_task, "shutdown task", 4* 1024, NULL, 1, NULL);

//	static movement_param_t *movement_param;
//	static int num_block;
//	movement_param = process_data(data, 44, &num_block);
//	for (int i = 0; i < num_block; i++) {
//		printf(" %d, %d, %d \n", movement_param[i].id, movement_param[i].param1,
//				movement_param[i].param2);
//	}
//	printf("ID is %d \n", movement_param[0]->id);

}




static void main_program(void *param) {

	event_msg_t event_msg;
	printf("start main\n");
	while (1) {
		Message.listen_event(&event_msg);
		printf("Id is: %d\n", event_msg.movement_papram.id);
	}
}

void start_main_program(void) {
	xTaskCreate(main_program, "manin", 2 * 1024, NULL, 1, NULL);
}
