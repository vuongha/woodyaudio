/*
 * taskBLE.cpp
 *
 *  Created on: Oct 16, 2019
 *      Author: TA QUOC ANH
 */
#include <BLEDevice.h>
#include <BLEUtils.h>
#include <BLEServer.h>
#include "iostream"
#include <string.h>
#include <stdio.h>
#include "driver/gpio.h"
#include <driver/i2c.h>
#include <esp_log.h>
#include <esp_err.h>
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "sdkconfig.h"
#include "GBBLightRGB.h"
#include "GBBBattery.h"
#include "GBBMotor.h"
#include "GBBOTA.h"
#include <BLE2902.h>
#include "parson.h"
#include "taskBLE.h"
#include "manage.h"

#define BASE 						"BASE"
#define FIRMWARE_SERVICE 			"FIRMWARE_SERVICE"
#define NEW_FIRMWARE_VERSION	 	"NEW_FIRMWARE_VERSION"
#define BLE_WIFI_SSID 				"WIFI_SSID"
#define READY_TO_UPDATE 			"READY_TO_UPDATE"
#define FIRMWARE_URL_STRING 		"FIRMWARE_URL_STRING"
#define BLE_WIFI_PASSWORD 			"WIFI_PASSWORD"
#define CURRENT_FIRMWARE_VERSION 	"CURRENT_FIRMWARE_VERSION"

#define SERVICE_UUID_AD      "62610000-f5ea-490e-8548-e6821e3e7792"

/**
 * Block program Service
 */

#define SERVICE_UUID_BLOCK_PROGRAM         "6261CB01-F5EA-490E-8548-E6821E3E7792"
#define CHAR_BLOCK_UNIT      			   "62614C14-F5EA-490E-8548-E6821E3E7792"
#define CHAR_BLOCK_PROGRAM  				"6261EC06-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_BLOCK_STATUS				"6261C024-F5EA-490E-8548-E6821E3E7792"

/**
 * Battery Service
 */

#define SERVICE_UUID_BATEERRY_SERVICE			    "62617F3E-F5EA-490E-8548-E6821E3E7792"

#define CHAR_UUID_BATTERRY_LEVEL					"6261843D-F5EA-490E-8548-E6821E3E7792"

/**
 * Information Service
 */

#define SERVICE_UUID_MONTE_DEVICE_INFORMATION 	"6261A894-F5EA-490E-8548-E6821E3E7792"

#define CHAR_UUID_FIRMWARE_VERSION 			"62610EEA-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_HARDWARE_VERSION 			"62618B98-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_SERIAL_NUMBER 			"6261FC91-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_MODEL_NUMBER 				"626156A5-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_MODEL_NAME 				"62616FDD-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_MONTE_DEVICE_NAME 		"6261FBCB-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_MANUFACTURER 				"62615CD7-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_DEVICE_MACADDRESS 		"626162BD-F5EA-490E-8548-E6821E3E7792"

/**
 * Configure Service
 */

#define SERVICE_UUID_CONFIG			    "62613EE7-F5EA-490E-8548-E6821E3E7792"

#define CHAR_UUID_INFO_WIFI_OTA 		"6261AFB0-F5EA-490E-8548-E6821E3E7792"
#define CHAR_UUID_CONFIG                "6261EF81-F5EA-490E-8548-E6821E3E7792"

#define CHAR_UUID_BLE_OTA 				"626144B5-F5EA-490E-8548-E6821E3E7792"

#define FIRMWARE_VERSION 		 "1.0"
#define HARDWARE_VERSION 			"2.0"
#define SERIAL_NUMBER 			"123456"
#define MODEL_NUMBER 			"WD-01/VN"
#define MODEL_NAME 				"Woody"
#define MONTE_DEVICE_NAME 		"Woody"
#define MANUFACTURER 			"KODIMO Co., Ltd"
#define ADDRESS 				"240AC4A228D2"

#define SPACE_STORAGE "woody"
#define SSID          "ssid"
#define PASSWORD      "pass"
#define SERVER_IP_OTA "ipota"
#define SERVER_PORT_OTA "port"
#define SERVER_PATH_OTA "path"
#define PARTTION_EXECUTE_OTA "parttion"
#define NVS_CONFIG_ANGLE "angle"

volatile bool flag_stop = 0;

volatile bool flag_repeat = 0;

volatile bool flag_not_next_block = 1;

volatile bool flag_run_task_rotobt_play_music = 0;

volatile bool flag_state_robot_run_music = 0;

/*
 * Char Program service
 */
BLECharacteristic *pCharacteristic_Unit;
BLECharacteristic *pCharacteristic_Program;
BLECharacteristic *pCharacterristic_Block_Status;

/*
 * Char for OTA
 */

BLECharacteristic *pCharacteristic_URL;

/*
 * Char battery
 */
BLECharacteristic *pCharacteristic_Battery;
BLECharacteristic *pCharacteristic_Config;

/*
 * Char device information
 */

BLECharacteristic *pChar_Firmware_Verison;
BLECharacteristic *pChar_Hardware_Version;
BLECharacteristic *pChar_Serial_Number;
BLECharacteristic *pChar_Model_Number;
BLECharacteristic *pChar_Model_Name;
BLECharacteristic *pChar_Device_Name;
BLECharacteristic *pChar_Manufacturer;
BLECharacteristic *pChar_Address;
BLECharacteristic *pChar_Test;

static uint16_t mHendle;

using namespace std;

xQueueHandle Queue_Recive_Program;

xQueueHandle Queue_Param;

static void task_feed_comand(void *param);

movement_param_t* process_data(char *ptr_char, int len, int *num_block);

typedef struct {
	int mask_block;
	int value_block1;
	int value_block2;
} param_block;

typedef enum {
	PROGRAM = 0, SINGLE = 1
} type_program;

typedef struct {
	char *prepare_buf;
	int prepare_len;
	type_program type;
} Recive_Block;

class CallbackConnect: public BLEServerCallbacks {

	/*
	 * This function will be called when device disconnect
	 */
	void onDisconnect(BLEServer *pBLEServer) {
		RobotLED.runRGB(NO_CONNECT, 4);
		pCharacteristic_Program->setValue("R");
//		RobotBuzzer.playMidiSound(disconnected, 7);
		vTaskDelay(1000 / portTICK_PERIOD_MS);
//		RobotBuzzer.waitComplete();
//		flag_stop = true;
//		flag_repeat = false;
		vTaskDelay(700 / portTICK_PERIOD_MS);
		esp_restart();
	}

	/*
	 * This function will be called when device connect
	 */
	void onConnect(BLEServer *pBLEServer) {
//		RobotLED.stopRGBCurrent();
//		RobotLED.waitcomplete();
		vTaskDelay(100 / portTICK_PERIOD_MS);
		RobotLED.runRGB(BLUE, 4);
//		RobotBuzzer.playMidiSound(connected, 5);
//		RobotWheelBody.turnLeftAngle(10);
		vTaskDelay(250 / portTICK_PERIOD_MS);
		printf("Connected\n");
//		RobotWheelBody.turnRightAngle(10);
	}
};

class cbCharacteristic_BlockUnit: public BLECharacteristicCallbacks {

	/*
	 * - This function will be called when recived block (data) uint from other device (Magic wand)
	 * - After recived block (data) . Data will be send to execute_block_task() over QueueSend()
	 * - Then block is executed
	 */

	void onWrite(BLECharacteristic *pCharacteristic) {
		Recive_Block Var;
		std::string value = pCharacteristic->getValue();
//		std::size_t found = value.find("9999");
//		if (found != std::string::npos) {
//			sscanf(value.c_str(), "%d;%d;", &data[0], &data[1]);
//			mHendle = data[1];
//		} else {
//			if (mHendle == pCharacteristic->getHandle()) {
//				std::size_t cmdStop = value.find("100;");
//
//				if (cmdStop != std::string::npos) {
//					flag_stop = true;
//					flag_repeat = false;
//				}
//				if (flag_not_next_block == true) {
//					char *data = (char*) calloc(value.length() + 1,
//							sizeof(char));
//					memcpy(data, value.c_str(), value.length() + 1);
//					Var.prepare_buf = data;
//					Var.prepare_len = value.length() + 1;
//					xQueueSend(Queue_Recive_Unit, &Var, 100);
//				}
//			} else {
//				printf("Wrong mHendle:%d  getHandle:%d \r\n", mHendle,
//						pCharacteristic->getHandle());
//			}
//		}
		char *data = (char*) calloc(value.length() + 1, sizeof(char));
		memcpy(data, value.c_str(), value.length() + 1);
		Var.prepare_buf = data;
		Var.prepare_len = value.length() + 1;
		Var.type = SINGLE;
		xQueueSend(Queue_Recive_Program, &Var, 100);
		printf("reset\n");

	}
};

class cbCharacteristic_Program: public BLECharacteristicCallbacks {
	/*
	 * - This function will be called when recived block (data) uint from other device (Magic wand)
	 * - After recived block (data) . Data will be send to execute_block_task() over QueueSend()
	 * - Then block is executed
	 */
	void onWrite(BLECharacteristic *pCharacteristic) {
		Recive_Block Var;
		int data[6];
		std::string value = pCharacteristic->getValue();
//		std::size_t found = value.find("9999");
//		if (found != std::string::npos) {
//			sscanf(value.c_str(), "%d;%d;", &data[0], &data[1]);
//			mHendle = data[1];
//		} else {
//			if (mHendle == pCharacteristic->getHandle()) {
//				if (flag_not_next_block == true) {
//
//					std::size_t cmdRepeat = value.find("1001");
//					if (cmdRepeat != std::string::npos) {
//						flag_repeat = true; // enable repeat;
//					}
//
//					char *memory = (char*) calloc(value.length() + 1,
//							sizeof(char));
//					memcpy(memory, value.c_str(), value.length() + 1);
//					Var.prepare_buf = memory;
//					Var.prepare_len = value.length() + 1;
//					xQueueSend(Queue_Recive_Block, &Var, 100);
//				}
//			} else {
//				printf(
//						"pCharacteristic_Program: Wrong mHendle:%d  getHandle:%d \r\n",
//						mHendle, pCharacteristic->getHandle());
//			}
//		}

		char *memory = (char*) calloc(value.length() + 1, sizeof(char));
		memcpy(memory, value.c_str(), value.length() + 1);
		Var.prepare_buf = memory;
		Var.prepare_len = value.length() + 1;
		Var.type = PROGRAM;
		xQueueSend(Queue_Recive_Program, &Var, 100);
		printf("reset\n");

	}
};

class cbCharacteristic_URL: public BLECharacteristicCallbacks {

	/*
	 * This function parse string URL to store information for OTA over Wifi
	 * - Information include
	 * + SSID Wifi AP
	 * + Pass word Wifi AP
	 * + IP Server that file OTA.bin store
	 * + Port : default port is 80
	 * + Path : Path store file.bin
	 *
	 */
	void onWrite(BLECharacteristic *pCharacteristic) {

		std::string value = pCharacteristic->getValue();
		JSON_Value *root_value;
		JSON_Object *commit;
		char *url = (char*) calloc(value.length(), sizeof(char));
		memcpy(url, value.c_str(), value.length());
		printf("URL is %s \n", url);

		root_value = json_parse_string(url);
		if (json_value_get_type(root_value) != JSONObject) {
			printf("return\n");
			return;
		}
		commit = json_value_get_object(root_value);
		printf("ssid: %s \n pass: %s \n ip: %s \n port: %s \n path: %s \n",
				json_object_get_string(commit, "ssid"),
				json_object_get_string(commit, "pass"),
				json_object_get_string(commit, "ip"),
				json_object_get_string(commit, "port"),
				json_object_get_string(commit, "path"));

		printf("Config Information for OTA \n");
		esp_err_t err;
		nvs_handle store_handle;

		err = nvs_open(SPACE_STORAGE, NVS_READWRITE, &store_handle);
		if (err != ESP_OK) {
			printf("Can not open SPACE_STORAGE\n");
		} else {
			err = nvs_set_str(store_handle, SSID,
					json_object_get_string(commit, "ssid"));
			if (err != ESP_OK) {
				printf("Faild parttion \n");
			}
			printf("Successfull write ssid\n");

			err = nvs_set_str(store_handle, PASSWORD,
					json_object_get_string(commit, "pass"));
			if (err != ESP_OK) {
				printf("Faild parttion \n");
			}
			printf("Successfull write password\n");

			err = nvs_set_str(store_handle, SERVER_IP_OTA,
					json_object_get_string(commit, "ip"));
			if (err != ESP_OK) {
				printf("Faild parttion\n");
			}
			printf("Successfull write ip\n");

			err = nvs_set_str(store_handle, SERVER_PORT_OTA,
					json_object_get_string(commit, "port"));
			if (err != ESP_OK) {
				printf("Faild parttion\n");
			}
			printf("Successfull write port\n");

			err = nvs_set_str(store_handle, SERVER_PATH_OTA,
					json_object_get_string(commit, "path"));
			if (err != ESP_OK) {
				printf("Faild parttion\n");
			}
			printf("Successfull write path\n");
		}

		err = nvs_commit(store_handle);
		printf((err != ESP_OK) ? "Failed commit!\n" : "Done commit\n");
		nvs_close(store_handle);
		printf("OTA over wifi start\n");
//		RobotBuzzer.playMidiSound(ota_sound, 5);
		vTaskDelay(500 / portTICK_PERIOD_MS);
		RobotOta.execute_ota();

	}
};
class cbCharacteristic_New_Verson: public BLECharacteristicCallbacks {
	void onWrite(BLECharacteristic *pCharacteristic) {

		std::string value = pCharacteristic->getValue();
		printf("pCharacteristic_Program:%s\r\n", value.c_str());
	}

};

class cbCharacteristic_Curent_Verson: public BLECharacteristicCallbacks {
	void onWrite(BLECharacteristic *pCharacteristic) {

		std::string value = pCharacteristic->getValue();
		printf("pCharacteristic_Program:%s\r\n", value.c_str());
	}
};

class cbCharacteristic_Config_Angle: public BLECharacteristicCallbacks {
	/*
	 * This function process and store value step for rotate motor
	 * Recive data -> parse data -> store value into nvs table -> end
	 */
	void onWrite(BLECharacteristic *pCharacteristic) {

		std::string value = pCharacteristic->getValue();
		printf("Config value %s \n", value.c_str());
		int value_angle = atoi(value.c_str());
		printf("value is %d \n", value_angle);
		uint16_t new_value = (uint16_t) ((90 * 990) / (value_angle / 4));

		if ((new_value > 1200) || (new_value < 800))
			return;

		esp_err_t err;
		nvs_handle store_handle;
		err = nvs_open(SPACE_STORAGE, NVS_READWRITE, &store_handle);

		if (err != ESP_OK) {
			printf("Can not open SPACE_STORAGE\n");
		} else {
			err = nvs_set_u16(store_handle, NVS_CONFIG_ANGLE,
					(uint16_t) (new_value));
			if (err != ESP_OK) {
				printf("Faild parttion \n");
			}
			printf("Successfull write config angle value\n");
		}

		err = nvs_commit(store_handle);
		printf((err != ESP_OK) ? "Failed commit!\n" : "Done commit\n");
		nvs_close(store_handle);

	}
};

void start_ble_services(void) {

	BLEDevice::init("Moody");
	BLEServer *pServer = BLEDevice::createServer();
	pServer->setCallbacks(new CallbackConnect());

	/*
	 * Create service Block program
	 */
	BLEService *pService = pServer->createService(
			BLEUUID(SERVICE_UUID_BLOCK_PROGRAM));

	pCharacteristic_Program = pService->createCharacteristic(

	CHAR_BLOCK_PROGRAM,
			BLECharacteristic::PROPERTY_READ
					| BLECharacteristic::PROPERTY_WRITE);

	pCharacteristic_Unit = pService->createCharacteristic(
	CHAR_BLOCK_UNIT,
			BLECharacteristic::PROPERTY_READ
					| BLECharacteristic::PROPERTY_WRITE);

	pCharacteristic_Program->setValue("B");

	pCharacteristic_Program->setCallbacks(new cbCharacteristic_Program);

	pCharacteristic_Unit->setCallbacks(new cbCharacteristic_BlockUnit);

//	pCharacterristic_Block_Status = pService->createCharacteristic(
//	CHAR_UUID_BLOCK_STATUS,
//			BLECharacteristic::PROPERTY_NOTIFY
//					| BLECharacteristic::PROPERTY_READ);

//	pCharacterristic_Block_Status->addDescriptor(new BLE2902());

	pService->start();
	/*
	 * Create service to config URL for OTA firmware
	 */
//
//	BLEService *pService_Config = pServer->createService(
//			BLEUUID(SERVICE_UUID_CONFIG));
//
//	pCharacteristic_URL = pService_Config->createCharacteristic(
//	CHAR_UUID_INFO_WIFI_OTA, BLECharacteristic::PROPERTY_WRITE);
//
//	pCharacteristic_URL->setCallbacks(new cbCharacteristic_URL);
//
//	pCharacteristic_Config = pService_Config->createCharacteristic(
//	CHAR_UUID_CONFIG, BLECharacteristic::PROPERTY_WRITE);
//
//	pCharacteristic_Config->setCallbacks(new cbCharacteristic_Config_Angle);
//
//	pService_Config->start();

	/*
	 * Create device Information service and characteristics
	 */

//	BLEService *pService_Info_Device = pServer->createService(
//			BLEUUID(SERVICE_UUID_MONTE_DEVICE_INFORMATION));
//
//	pChar_Firmware_Verison = pService_Info_Device->createCharacteristic(
//	CHAR_UUID_FIRMWARE_VERSION, BLECharacteristic::PROPERTY_READ);
//	pChar_Firmware_Verison->setValue(FIRMWARE_VERSION);
//
//	pChar_Hardware_Version = pService_Info_Device->createCharacteristic(
//	CHAR_UUID_HARDWARE_VERSION, BLECharacteristic::PROPERTY_READ);
//	pChar_Hardware_Version->setValue(HARDWARE_VERSION);
//
//	pChar_Serial_Number = pService_Info_Device->createCharacteristic(
//	CHAR_UUID_SERIAL_NUMBER, BLECharacteristic::PROPERTY_READ);
//	pChar_Serial_Number->setValue(SERIAL_NUMBER);
//
//	pChar_Model_Number = pService_Info_Device->createCharacteristic(
//	CHAR_UUID_MODEL_NUMBER, BLECharacteristic::PROPERTY_READ);
//	pChar_Model_Number->setValue(MODEL_NUMBER);
//
//	pChar_Model_Name = pService_Info_Device->createCharacteristic(
//	CHAR_UUID_MODEL_NAME, BLECharacteristic::PROPERTY_READ);
//	pChar_Model_Name->setValue(MODEL_NAME);
//
//	pChar_Manufacturer = pService_Info_Device->createCharacteristic(
//	CHAR_UUID_MANUFACTURER, BLECharacteristic::PROPERTY_READ);
//	pChar_Manufacturer->setValue(MANUFACTURER);
//
//	pChar_Address = pService_Info_Device->createCharacteristic(
//	CHAR_UUID_DEVICE_MACADDRESS, BLECharacteristic::PROPERTY_READ);
//	pChar_Address->setValue(ADDRESS);
//
//	pService_Info_Device->start();
//
//	/*
//	 * Create Battery service
//	 */
//
//	BLEService *pService_Batt = pServer->createService(
//			BLEUUID(SERVICE_UUID_BATEERRY_SERVICE));
//	pCharacteristic_Battery = pService_Batt->createCharacteristic(
//	CHAR_UUID_BATTERRY_LEVEL,
//			BLECharacteristic::PROPERTY_READ
//					| BLECharacteristic::PROPERTY_NOTIFY
//					| BLECharacteristic::PROPERTY_INDICATE);
//
//	pCharacteristic_Battery->addDescriptor(new BLE2902());
//	pService_Batt->start();

	/*
	 * Create advertising service
	 */

	BLEAdvertising *pAdvertising = pServer->getAdvertising();

	pAdvertising->addServiceUUID(BLEUUID(SERVICE_UUID_AD));

	pAdvertising->start();

}

void start_task_ble(void) {

	Queue_Recive_Program = xQueueCreate(3, sizeof(Recive_Block));
	Queue_Param = xQueueCreate(3, sizeof(param_block));
	xTaskCreate(task_feed_comand, "Task feed", 2 * 2048, NULL, 1, NULL);

}



static void task_feed_comand(void *param) {
	static Recive_Block Var;
	static event_msg_t event_msg;
	static movement_param_t *movement_param;
	static int num_block;

	while (1) {
		xQueueReceive(Queue_Recive_Program, &Var, portMAX_DELAY);
		switch (Var.type) {
		case SINGLE:
			sscanf(Var.prepare_buf, "%d,%d,%d", &event_msg.movement_papram.id,
					&event_msg.movement_papram.param1,
					&event_msg.movement_papram.param2);
			event_msg.source = BLE;
			event_msg.action = MOVEMENT;
			Message.send_event(&event_msg);
			printf(" single queue recived\n");
			free(Var.prepare_buf);

			break;
		case PROGRAM:
			printf(" program queue recived\n");
			free(Var.prepare_buf);
			movement_param = process_data(Var.prepare_buf, Var.prepare_len,
					&num_block);

			for (int i = 0; i < num_block; i++) {
				printf("Data is: %d, %d, %d \n", movement_param[i].id,
						movement_param[i].param1, movement_param[i].param2);
			}

			break;
		default:
			break;
		}
	}
}

void find_index_block(char *param, int len, int *index) {
	int i = 0;
	for (i = 0; i < len; i++) {
		if (param[i] == ';') {
			*(index) = i;
			index = index + 1;
		}
	}
}

movement_param_t* process_data(char *ptr_char, int len, int *num_block) {

	char char_value[1];
	static char *array_block;
	int num_of_block = 0;

	array_block = ptr_char;

	for (int i = 0; i < len; i++) {
		if (array_block[i] == ';')
			num_of_block++;
	}
	num_of_block--;
//	printf("Num of block %d \n", num_of_block);
//	if (num_of_block < 3) {
//		flag_repeat = false;
//		flag_stop = true;
//		return;
//	}

	int *index_block = (int*) calloc(num_of_block, sizeof(int));
	movement_param_t *movement_array = (movement_param_t*) calloc(num_of_block,
			sizeof(movement_param_t));


	find_index_block((char*) array_block, len, index_block);

	for (int i = 0; i < num_of_block; i++) {
		sscanf((char*) (array_block + index_block[i] + 1), "%d,%d,%d",
				&movement_array[i].id, &movement_array[i].param1,
				&movement_array[i].param2);
	}
	free(array_block);
	*num_block = num_of_block;
	return movement_array;

}

taskBLE::taskBLE() {
	// TODO Auto-generated constructor stub

}

taskBLE::~taskBLE() {
	// TODO Auto-generated destructor stub
}

