/*
 * Express.cpp
 *
 *  Created on: Oct 21, 2019
 *      Author: TA QUOC ANH
 */

#include "Express.h"
#include "sdkconfig.h"
#include <string.h>
#include <stdio.h>
#include "driver/gpio.h"
#include <driver/i2c.h>
#include <esp_log.h>
#include <esp_err.h>

Express::Express() {
	queue = xQueueCreate(2, sizeof(event_msg_t));

}

Express::~Express() {
	// TODO Auto-generated destructor stub
}

void Express::send_event(event_msg_t *event) {
	xQueueSend(queue, event, 100);
}

void Express::listen_event(event_msg_t *event) {
	xQueueReceive(queue, event, portMAX_DELAY);
}


Express Message;
